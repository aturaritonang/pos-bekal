﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.ViewModel
{
    public class Product
    {
        public Product()
        {
            Id = 0;
            Action = 0;

            CreateBy = "Atur";
            CreateDate = DateTime.Now;

            ModifyBy = "Atur";
            ModifyDate = DateTime.Now;
        }

        public int Id { get; set; }
        public string Initial { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public bool Active { get; set; }

        public int Action { get; set; }

        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime ModifyDate { get; set; }
    }
}
