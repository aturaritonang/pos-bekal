﻿using POS.Utility;
using POS.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Repository
{
    public class OrderRepo
    {
        public static bool PostOrder(OrderHeader oh)
        {
            bool result = true;
            SqlCommand sqlComm = new SqlCommand();
            using (SqlConnection conn = new SqlConnection(DbConnections.DatabaseConnectionString()))
            {
                try
                {
                    using (var table = new DataTable())
                    {
                        string newReference = NewReference();

                        conn.Open();
                        sqlComm.Connection = conn;
                        sqlComm.CommandType = CommandType.StoredProcedure;
                        sqlComm.CommandText = "sp_OrderPost";

                        sqlComm.Parameters.AddWithValue("@pReference", newReference);
                        sqlComm.Parameters.AddWithValue("@pAmount", oh.Amount);
                        sqlComm.Parameters.AddWithValue("@pActive", oh.Active);
                        sqlComm.Parameters.AddWithValue("@pCreateBy", oh.CreateBy);

                        table.Columns.Add("ProductId", typeof(int));
                        table.Columns.Add("Quantity", typeof(decimal));
                        table.Columns.Add("Price", typeof(decimal));
                        table.Columns.Add("Active", typeof(bool));

                        foreach (var item in oh.List)
                        {
                            DataRow dr = table.NewRow();
                            dr["ProductId"] = item.ProductId;
                            dr["Quantity"] = item.Quantity;
                            dr["Price"] = item.Price;
                            dr["Active"] = item.Active;

                            table.Rows.Add(dr);
                        }

                        sqlComm.Parameters.AddWithValue("@pOrderDetails", table);
                        sqlComm.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                }
            }
            return result;
        }

        public static string NewReference()
        {
            string yearMonth = DateTime.Now.ToString("yy") + DateTime.Now.Month.ToString("D2");
            string result = "SLS-" + yearMonth + "-";

            SqlCommand sqlComm = new SqlCommand();
            SqlDataReader sqlDr;

            using (SqlConnection conn = new SqlConnection(DbConnections.DatabaseConnectionString()))
            {
                try
                {
                    conn.Open();
                    sqlComm.Connection = conn;
                    sqlComm.CommandType = CommandType.Text;
                    sqlComm.CommandText = "SELECT ISNULL(MAX(Reference), '" + result + "0000') Reference FROM OrderHeader WHERE Reference Like '" + result + "%'";

                    sqlDr = sqlComm.ExecuteReader();
                    if (sqlDr.HasRows)
                    {
                        while (sqlDr.Read())
                        {
                            string lastReff = sqlDr["Reference"].ToString();
                            string[] arrLastReff = lastReff.Split('-');
                            int newIncr = int.Parse(arrLastReff[2]) + 1;
                            result += newIncr.ToString("D4");
                            break;
                        }
                    }
                    else
                    {
                        result += "0001";
                    }

                }
                catch (Exception ex)
                {
                    result += "0001";
                    throw;
                }
            }
            return result;
        }
    }
}
