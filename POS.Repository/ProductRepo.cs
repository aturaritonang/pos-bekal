﻿using POS.Utility;
using POS.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Repository
{
    public class ProductRepo
    {
        public static Product GetById(int id)
        {
            Product result = new Product();
            var list = GetAll(id);
            if (list.Count > 0)
            {
                result = list[0];
            }
            return result;
        }

        public static List<Product> GetAll()
        {
            return GetAll(0);
        }

        public static List<Product> GetAll(int productId)
        {
            List<Product> result = new List<Product>();
            SqlCommand sqlComm = new SqlCommand();
            SqlDataReader sqlDr;

            using (SqlConnection conn = new SqlConnection(DbConnections.DatabaseConnectionString()))
            {
                try
                {
                    conn.Open();
                    sqlComm.Connection = conn;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.CommandText = "sp_MstProductGetAll";

                    sqlComm.Parameters.AddWithValue("@pId", productId);

                    sqlDr = sqlComm.ExecuteReader();
                    if (sqlDr.HasRows)
                    {
                        while (sqlDr.Read())
                        {
                            Product product = new Product();
                            product.Id = Convert.ToInt32(sqlDr["Id"]);
                            product.Initial = sqlDr["Initial"].ToString();
                            product.Name = sqlDr["Name"].ToString();
                            product.Description = sqlDr["Description"].ToString();
                            product.Price = Convert.ToDecimal(sqlDr["Price"]);
                            product.Active = Convert.ToBoolean(sqlDr["Active"]);

                            product.CreateDate = Convert.ToDateTime(sqlDr["CreateDate"]);
                            product.CreateBy = sqlDr["ModifyDate"].ToString();
                            product.ModifyDate = Convert.ToDateTime(sqlDr["CreateDate"]);
                            product.ModifyBy = sqlDr["ModifyBy"].ToString();

                            result.Add(product);
                        }
                        sqlDr.Close();
                    }
                }

                catch (Exception ex)
                {

                }

                finally
                {
                    if (conn.State == ConnectionState.Open)
                    {
                        conn.Close();
                    }
                }
            }
            return result;
        }

        public static bool Update(Product model)
        {
            bool result = true;
            SqlCommand sqlComm = new SqlCommand();
            using (SqlConnection conn = new SqlConnection(DbConnections.DatabaseConnectionString()))
            {
                try
                {
                    conn.Open();
                    sqlComm.Connection = conn;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.CommandText = "sp_MstProductUpdate";

                    sqlComm.Parameters.AddWithValue("@pId", model.Id);

                    sqlComm.Parameters.AddWithValue("@pInitial", model.Initial);
                    sqlComm.Parameters.AddWithValue("@pName", model.Name);
                    sqlComm.Parameters.AddWithValue("@pDescription", model.Description);
                    sqlComm.Parameters.AddWithValue("@pPrice", model.Price);
                    sqlComm.Parameters.AddWithValue("@pActive", model.Active);

                    sqlComm.Parameters.AddWithValue("@pAction", model.Action);

                    sqlComm.Parameters.AddWithValue("@pCreateBy", "Atur");
                    sqlComm.Parameters.AddWithValue("@pModifyBy", "Atur");

                    sqlComm.ExecuteNonQuery();
                }
                finally
                {
                    if (conn.State == ConnectionState.Open)
                    {
                        conn.Close();
                    }
                }
            }
            return result;
        }
    }
}
