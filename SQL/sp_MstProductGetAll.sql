SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MstProductGetAll] 
@pId int = 0
AS
BEGIN
	SELECT Id, Initial, Name, Description, Price, Active, CreateBy, CreateDate, ModifyBy, ModifyDate
	FROM MstProduct
	WHERE CASE WHEN @pId = 0 THEN Id ELSE @pId END = Id
	ORDER BY Initial, Name
END

GO

