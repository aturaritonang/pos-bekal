SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_OrderPost]
@pReference nchar(15),
@pAmount decimal(18,2) = 0,
@pActive bit = 1, 
@pCreateBy varchar(50) = '',
@pOrderDetails OrderDetailType READONLY
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;
	DECLARE @NewID INT

    BEGIN TRANSACTION;
		
	BEGIN TRY
		INSERT INTO OrderHeader (Reference, Amount, Active, CreateBy, CreateDate, ModifyBy, ModifyDate)
		VALUES (@pReference, @pAmount, @pActive, @pCreateBy, GETDATE(), @pCreateBy, GETDATE())

		SET @NewID = CAST(scope_identity() AS int)

		INSERT INTO OrderDetail (HeaderId, ProductId, Quantity, Price, Active, CreateBy, CreateDate, ModifyBy, ModifyDate)
		SELECT @NewID, pOD.ProductId, pOD.Quantity, pOD.Price, 1, @pCreateBy, GETDATE(), @pCreateBy, GETDATE() FROM @pOrderDetails AS pOD

		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH

		ROLLBACK TRANSACTION;
	END CATCH;
END

GO

