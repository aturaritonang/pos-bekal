SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MstProductUpdate] 
@pId Int = 0,
@pInitial Char(10) = '',
@pName VarChar(50) = '',
@pDescription VarChar(500) = '',
@pPrice Decimal(18,2) = 0,
@pActive bit = 0, 
@pCreateBy VarChar(50) = '', 
@pModifyBy VarChar(50) = '',
@pAction TinyInt = 0
-- 0 Insert & Update, 1 Delete
AS
BEGIN
	IF @pAction = 0
		BEGIN
			-- INSERT
			IF @pId = 0
				BEGIN
					INSERT INTO MstProduct 
					(Initial, Name, Description, Price, Active, CreateBy, CreateDate, ModifyBy, ModifyDate)
					VALUES 
					(@pInitial, @pName, @pDescription, @pPrice, @pActive, @pCreateBy, GETDATE(), @pModifyBy, GETDATE())
				END
			ELSE
			-- UPDATE
				BEGIN
					UPDATE MstProduct SET Initial = @pInitial, Name = @pName, Description = @pDescription, Price = @pPrice, Active = @pActive, ModifyBy = @pModifyBy, ModifyDate = GETDATE()
					WHERE Id = @pId
				END
		END
	ELSE
		-- DELETE
		BEGIN
			DELETE MstProduct WHERE Id = @pId
		END
END

GO

